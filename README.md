# LocalPubSub

A sample Elixir project that shows basic integration with Google PubSub running on a local emulator in an iex session.

## Pre-requisites

Install the local emulator by running the following commands:

```
gcloud components install pubsub-emulator
gcloud components update
gcloud beta emulators pubsub start --project=my-project
```

## Running

The application is meant to be run in iex.  From the root directory run: `iex -S mix` and within an Interactive Elixir session run the following:

```
iex(1)> Main.run()
Project ID?: my-project
Topic Name?: sample-project
Topic does not exist. Create it? (y/n): y
created projects/my-project/topics/sample-project
Subscription Name?: sample-subscription
Subscription does not exist. Create it? (y/n): y
created subscription projects/my-project/subscriptions/sample-subscription on topic projects/my-project/subscriptions/sample-subscription
Enter a pubsub message, or (q) to quit: 
Test message
published message 21
Enter a pubsub message, or (q) to quit: 
received and acknowledged message: Test message
Enter a pubsub message, or (q) to quit: 
q
nil
```

In the session you can choose whatever topic and subscription name you like, and send whatever messages you like and they should immediately be consumed in a separate process.

## Caveats

If you quit the session you will not be able to restart it with `Main.run()` again without restarting the Interactive Elixir session.

The code in here is only setup for running with a local pubsub emulator, but with some configuration could be setup to run against a real Google Pubsub environment.  To do so you would need to add the service account JSON credential file and set the `GOOGLE_APPLICATION_CREDENTIALS` environment variable to that file.  Also there is no docker setup in this project and this project assumes you'll run Elixir and the pubsub emulator directly on your host machine.

## See also

https://cloud.google.com/pubsub/docs/emulator for more information on running an emulator

https://hub.docker.com/r/bigtruedata/gcloud-pubsub-emulator for running the pubsub emulator in a Docker container.

https://github.com/GoogleCloudPlatform/elixir-samples/tree/master/pubsub has most of the code that was running here.  It was slightly refactored and modified for this example to get it running locally.