defmodule Main do

  import PubSubClient

  @doc """
  Runs a simple example which executes a pubsub supervisor and publishes to a
  PubSub topic.
  ## Examples
      iex> Main.run
  """
  def run() do
    {:ok, _started} = Application.ensure_all_started(:goth)
    :httpc.set_options(pipeline_timeout: 1000)
    project_id = get_response("Project ID?: ")
    topic_name = get_response("Topic Name?: ")
    topic = get_or_create_topic(project_id, topic_name)
    if topic do
      subscription_name = get_response "Subscription Name?: "
      subscription = get_or_create_subscription(project_id, topic_name, subscription_name)
      if subscription do
        {:ok, _pid} = start_subscription_supervisor(project_id, subscription_name)
        get_next_message(project_id, topic_name)
      end
    end
  end

  defp start_subscription_supervisor(project_id, subscription_name) do
    import Supervisor.Spec

    listener = %Listener{
      project_id: project_id,
      subscription_name: subscription_name,
      handler: &MessagePrinter.print_message/1
    }

    # Define workers and child supervisors to be supervised
    children = [
      # Start the worker when the application starts
      supervisor(Listener, [listener]),
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MySupervisor]
    Supervisor.start_link(children, opts)
  end

  defp get_or_create_topic(project_id, topic_name) do
    PubSubClient.get_topic(project_id, topic_name) ||
      (confirm_topic_creation() && PubSubClient.create_topic(project_id, topic_name))
  end

  defp confirm_topic_creation do
    case get_response("Topic does not exist. Create it? (y/n): ") do
      "y" -> true
      _ -> nil
    end
  end

  defp get_or_create_subscription(project_id, topic_name, subscription_name) do
    PubSubClient.get_subscription(project_id, subscription_name) ||
      (confirm_subscription_creation() && PubSubClient.create_subscription(project_id, topic_name, subscription_name))
  end

  defp confirm_subscription_creation do
    case get_response("Subscription does not exist. Create it? (y/n): ") do
      "y" -> true
      _ -> nil
    end
  end

  defp get_next_message(project_id, topic_name) do
    message = get_response("Enter a pubsub message, or (q) to quit: \n")
    if !Enum.member?(["q", ""], message) do
      PubSubClient.publish(project_id, topic_name, message)
      get_next_message(project_id, topic_name)
    end
  end

  defp get_response(prompt) do
    prompt |> IO.gets() |> String.replace_trailing("\n", "")
  end

end
