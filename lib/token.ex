defmodule TokenWrapper do

  def get_token() do
    if Application.get_env(:local_pub_sub, :disabled, true) do
      {:ok, %{token: "local"}}
    else
      Goth.Token.for_scope("https://www.googleapis.com/auth/cloud-platform")
    end
  end
end
