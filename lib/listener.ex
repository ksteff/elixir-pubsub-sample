defmodule Listener do
  import TokenWrapper

  use Task

  @type t :: %__MODULE__{project_id: binary, subscription_name: binary, handler: function}
  defstruct [:project_id, :subscription_name, :handler]

  @moduledoc """
  Subscriber sample for GoogleApi.PubSub
  """

  @spec start_link(t) :: {:ok, pid}
  def start_link(listener) do
    task =
      Task.async(Listener, :listen, [
        listener.project_id,
        listener.subscription_name,
        listener.handler
      ])

    {:ok, task.pid}
  end

  @spec listen(binary, binary, function) :: no_return
  def listen(project_id, subscription_name, handler) do
    # Authenticate
    {:ok, token} = get_token()
    conn = GoogleApi.PubSub.V1.Connection.new(token.token)

    # Make a subscription pull
    {:ok, response} =
      GoogleApi.PubSub.V1.Api.Projects.pubsub_projects_subscriptions_pull(
        conn,
        project_id,
        subscription_name,
        body: %GoogleApi.PubSub.V1.Model.PullRequest{
          maxMessages: 10
        }
      )

    if response.receivedMessages != nil do
      Enum.each(response.receivedMessages, fn message ->
        case handler.(message) do
          {:ok, message} ->
            GoogleApi.PubSub.V1.Api.Projects.pubsub_projects_subscriptions_acknowledge(
              conn,
              project_id,
              subscription_name,
              body: %GoogleApi.PubSub.V1.Model.AcknowledgeRequest{
                ackIds: [message.ackId]
              }
            )

          {:error, error} ->
            IO.puts("Error handling message: #{error}")
        end
      end)
    end

    listen(project_id, subscription_name, handler)
  end
end

defmodule MessagePrinter do
  @spec print_message(atom | %{message: atom | %{data: binary}}) ::
          {:ok, atom | %{message: atom | map}}
  def print_message(message) do
    "received and acknowledged message: #{Base.decode64!(message.message.data)}"
    |> (&IO.ANSI.format([:green, :bright, &1])).()
    |> IO.puts()

    {:ok, message}
  end
end
